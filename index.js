// Advance Query Operators
// We want more flexible querying of data within MongoDB

// [SECTION] Comparison Query Operators
// $gt - greater than
// $gte - greater than or equal
/*
	Syntax:
	db.collectionName.find({field: {$gt/$gte: value}})
*/

db.users.find(
	{
		age:{$gt: 21}
	}
)

db.users.find(
	{
		age:{$gte: 21}
	}
)

// $lt - less than
// $lte - less than or equal

db.users.find(
	{
		age:{$lt: 82}
	}
)

db.users.find(
	{
		age:{$lte: 82}
	}
)

// $ne - not equal

db.users.find({	age:{$ne: 82} })

// $in 
/*
	- Allows us to find documents with specific math criteria with one field using different values
	Syntax:
	db.collectionName.find(field:{$in: [valueA, valueB]})
*/

db.users.find(
	{
		lastName:{
			$in: ['Hawking', 'Doe']
		}
	}
)

db.users.find(
	{
		courses: {
			$in: ["HTML", "React"]
		}
	}
)

// $or (1 true = true)
/*
	db.collectionName.find({$or:[{fieldA:valueA}, {fieldB:valueB}]})
*/

db.users.find(
	{
		$or: [
			{firstName: "Neil"},
			{age:21}
		]
	}
)

// $and (all should be satisfied)
/*
	db.collectionName.find({$and:[{fieldA:valueA}, {fieldB:valueB}]})
*/

db.users.find(
	{
		$and: [
			{age: {$ne: 82}},
			{age: {$ne: 76}}
		]
	}
)

// [SECTION] Field Projection
// To help with readability of the values returned, we can include/exclude fields from the response
/*
	Syntax:
	db.collectionName.find({criteria}, {fields you want to include, it should have a value of 1})
*/

// Inclusion (1)

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)

// Exclusion (0)

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
)

// Suppressing the ID field
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
)

// "errmsg" : "Cannot do exclusion on field department in inclusion projection"
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		department: 0
	}
)

// $regex

// Case sensitive
db.users.find(
	{
		firstName: {
			$regex: 'N'
		}
	}
)

// Not case sensitive
db.users.find(
	{
		firstName: {
			$regex: 'N', $options: '$i'
		}
	}
)


